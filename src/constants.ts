export interface MenuItem {
  label: string;
  url: string;
}

export const POSTS: MenuItem = {
  label: 'Posts',
  url: '/posts',
};

export const GUEST_BOOK: MenuItem = {
  label: 'Guest Book',
  url: '/guest-book',
};

export const SORT_VALUES = {
  asc: 'Ascend',
  desc: 'Descend',
};
