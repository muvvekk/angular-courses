import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import MaterialModules from '../../../modules/material.modules';

import { MenuVerticalComponent } from './menu-vertical.component';

describe('MenuVerticalComponent', () => {
  let component: MenuVerticalComponent;
  let fixture: ComponentFixture<MenuVerticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModules],
      declarations: [MenuVerticalComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuVerticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
