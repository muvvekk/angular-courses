import { Component, ChangeDetectionStrategy } from '@angular/core';

import { MenuItem, POSTS, GUEST_BOOK } from '../../../../constants';

@Component({
  selector: 'app-menu-vertical',
  templateUrl: './menu-vertical.component.html',
  styleUrls: ['./menu-vertical.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuVerticalComponent {
  public items: MenuItem[] = [
    POSTS,
    GUEST_BOOK,
  ];
}
