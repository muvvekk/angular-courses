import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Post, Comment } from '../../../services/posts/typings';
import { PostsService } from '../../../services/posts/posts.service';

import { POSTS } from '../../../../constants';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss'],
})
export class SinglePostComponent implements OnInit {
  public id: number;
  public url: string = POSTS.url;

  public post: Post;
  public postLoading = true;

  public comments: Comment[];
  public commentsLoading = true;

  constructor(private postsService: PostsService, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.postLoading = true;
    this.commentsLoading = true;

    this.postsService.getSingle(this.id).subscribe((post: Post) => {
      this.postLoading = false;
      this.post = post;
    });
    this.postsService.getComments(this.id).subscribe((comments: Comment[]) => {
      this.commentsLoading = false;
      this.comments = comments;
    });
  }
}
