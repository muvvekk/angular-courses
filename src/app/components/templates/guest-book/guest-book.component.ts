import {Component} from '@angular/core';

import {QuestBookFormData} from '../../molecules/quest-book-form/typing';

@Component({
  selector: 'app-guest-book',
  templateUrl: './guest-book.component.html',
  styleUrls: ['./guest-book.component.scss'],
})
export class GuestBookComponent {
  public records: QuestBookFormData[] = [];

  public appendRecords = (record: QuestBookFormData) => {
    this.records.push(record);
  }
}
