import { BehaviorSubject } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { Post } from '../../../services/posts/typings';

import { PostsService } from '../../../services/posts/posts.service';
import { POSTS, SORT_VALUES } from '../../../../constants';

@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.scss'],
})
export class AllPostsComponent implements OnInit {
  public sort = new BehaviorSubject<string>('');
  public posts = new BehaviorSubject<Post[]>([]);
  public loading = true;
  public url: string = POSTS.url;

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.loading = true;
    this.postsService.getAll().subscribe((posts: Post[]) => {
      this.loading = false;
      this.sortPostsById(posts);
    });
  }

  public sortPostsById = (posts = this.posts.value) => {
    const sort = this.sort.value === SORT_VALUES.asc ? SORT_VALUES.desc : SORT_VALUES.asc;

    posts = posts.sort((a, b) => (a.id < b.id ? -1 : 1));
    if (sort === SORT_VALUES.asc) {
      posts = posts.reverse();
    }

    this.sort.next(sort);
    this.posts.next(posts);
  };
}
