import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent {}
