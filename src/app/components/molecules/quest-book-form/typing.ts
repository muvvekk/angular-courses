export interface QuestBookFormData {
  name: string;
  body: string;
  email?: string;
}
