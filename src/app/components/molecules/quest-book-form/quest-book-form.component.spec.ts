import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import MaterialModules from '../../../modules/material.modules';

import { QuestBookFormComponent } from './quest-book-form.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('QuestBookFormComponent', () => {
  let component: QuestBookFormComponent;
  let fixture: ComponentFixture<QuestBookFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, MaterialModules, ReactiveFormsModule, FormsModule, BrowserAnimationsModule],
      declarations: [QuestBookFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestBookFormComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
