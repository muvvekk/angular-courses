import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';

import { QuestBookFormData } from './typing';

@Component({
  selector: 'app-quest-book-form',
  templateUrl: './quest-book-form.component.html',
  styleUrls: ['./quest-book-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestBookFormComponent {
  @Output() questBookRecord = new EventEmitter();

  public nameControl = new FormControl('', Validators.required);
  public emailControl = new FormControl('', Validators.email);
  public bodyControl = new FormControl('', [Validators.required, Validators.minLength(20)]);

  public data: FormGroup;

  constructor(formBuilder: FormBuilder) {
    this.data = formBuilder.group({
      name: this.nameControl,
      email: this.emailControl,
      body: this.bodyControl,
    });
  }

  public onSubmit = (): void => {
    const formData: QuestBookFormData = this.data.getRawValue();

    this.questBookRecord.emit(formData);

    this.data.disable();
    this.data.reset();
    this.data.enable();
  }
}
