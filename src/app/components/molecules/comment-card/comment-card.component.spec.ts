import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import MaterialModules from '../../../modules/material.modules';

import { CommentCardComponent, UserProfileModalContentComponent } from './comment-card.component';

describe('CommentCardComponent', () => {
  let component: CommentCardComponent;
  let fixture: ComponentFixture<CommentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, MaterialModules],
      declarations: [CommentCardComponent, UserProfileModalContentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentCardComponent);
    component = fixture.componentInstance;

    component.comment = {
      id: 1,
      postId: 1,
      name: 'John Smith',
      email: 'j.smith@gmail.com',
      body: 'My first text',
    };

    fixture.detectChanges();
  });

  it('should be created - without profile modal', () => {
    expect(component).toBeTruthy();
  });

  it('should be created - with profile modal', () => {
    component.showProfile = true;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('method should be called', () => {
    component.showProfile = true;
    fixture.detectChanges();

    spyOn(component, 'openProfileModal');

    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();

    fixture.whenStable().then(() => {
      expect(component.openProfileModal).toHaveBeenCalled();
    });
  });
});
