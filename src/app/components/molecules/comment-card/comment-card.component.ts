import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';

import { getGravatar } from '../../../utils/gravatar';
import { Comment, UserProfile } from '../../../services/posts/typings';

@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.scss'],
})
export class CommentCardComponent implements OnInit {
  @Input() comment: Comment;
  @Input() showProfile = false;

  public avatar: string;

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    this.avatar = getGravatar({ email: this.comment.email });
  }

  public openProfileModal = () => {
    const { name, email } = this.comment;

    this.dialog.open(UserProfileModalContentComponent, {
      width: '500px',
      data: {
        avatar: this.avatar,
        name,
        email,
      },
    });
  }
}

@Component({
  selector: 'app-user-profile-modal-content',
  templateUrl: './user-profile-modal-content.html',
  styleUrls: ['./comment-card.component.scss'],
})
export class UserProfileModalContentComponent {
  constructor(
    public dialogRef: MatDialogRef<UserProfileModalContentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserProfile
  ) {}
}
