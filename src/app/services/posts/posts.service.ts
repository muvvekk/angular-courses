import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post, Comment } from './typings';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  private url = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) { }

  public getAll = (): Observable<Post[]> => {
    return this.http.get<Post[]>(this.url);
  }

  public getSingle = (id: number): Observable<Post> => {
    return this.http.get<Post>(`${this.url}/${id}`);
  }

  public getComments = (postId: number): Observable<Comment[]> => {
    return this.http.get<Comment[]>(`${this.url}/${postId}/comments`);
  }
}
