import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Modules
import MaterialModules from './modules/material.modules';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxRatingModule } from '@mursaat/ngx-rating';
// Atoms
import { PostCardComponent } from './components/atoms/post-card/post-card.component';
import { MenuVerticalComponent } from './components/atoms/menu-vertical/menu-vertical.component';
// Molecules
import {
  CommentCardComponent,
  UserProfileModalContentComponent,
} from './components/molecules/comment-card/comment-card.component';
import { QuestBookFormComponent } from './components/molecules/quest-book-form/quest-book-form.component';
// Templates
import { ErrorComponent } from './components/templates/error/error.component';
import { AllPostsComponent } from './components/templates/all-posts/all-posts.component';
import { SinglePostComponent } from './components/templates/single-post/single-post.component';
import { GuestBookComponent } from './components/templates/guest-book/guest-book.component';
// Services
import { PostsService } from './services/posts/posts.service';

@NgModule({
  declarations: [
    AppComponent,
    AllPostsComponent,
    PostCardComponent,
    ErrorComponent,
    SinglePostComponent,
    MenuVerticalComponent,
    CommentCardComponent,
    GuestBookComponent,
    QuestBookFormComponent,
    UserProfileModalContentComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModules,
    NgxRatingModule,
    BrowserAnimationsModule,
  ],
  providers: [PostsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
