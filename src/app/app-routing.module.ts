import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllPostsComponent } from './components/templates/all-posts/all-posts.component';
import { ErrorComponent } from './components/templates/error/error.component';
import { SinglePostComponent } from './components/templates/single-post/single-post.component';
import { GuestBookComponent } from './components/templates/guest-book/guest-book.component';

const routes: Routes = [
  { path: '', redirectTo: '/posts', pathMatch: 'full' },
  {
    path: 'posts',
    children: [
      { path: '', component: AllPostsComponent },
      { path: ':id', component: SinglePostComponent },
    ]
  },
  { path: 'guest-book', component: GuestBookComponent },
  { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
