import gravatar from 'gravatar';

interface GravatarParams {
  email: string;
  size?: number;
}

export const getGravatar = ({email, size = 100}: GravatarParams) => gravatar.url(email, { s: size });
